#!/bin/bash
set -feuo pipefail

# d, a dc(1) interface for swifter interaction. Prepend $ to the
# argument list to drop into dc after processing the other arguments.

# Installation: Put this file (or a link to it) somewhere in your path
# with the defs.dc file in the same directory. It is also useful to
# turn off globbing and expansion from the shell as this will probably
# get in the way. Use noglob, set -f & their intimates.

# E.g., with zsh, add the following line to ~/.zshrc.local:
# alias d='noglob d'

# More fun:
# rlwrap -pYellow -S'dc> ' d $ 

readonly dfile=".defs.dc"
readonly defs="$(dirname "$(realpath "$0")")/$dfile"

interactive=""

if [[ "$1" == "$" ]]
then
    interactive="-"
    shift
fi

dc --file="$defs" --expression="$* f" $interactive
